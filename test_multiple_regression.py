import os
import csv
import unittest
import tempfile
import pandas as pd
from unittest.mock import patch
from io import StringIO
from multiple_regression import MultivariateRegression

class TestMultivariateRegression(unittest.TestCase):
    
    def setUp(self):
        self.multivar_reg = MultivariateRegression()

    def tearDown(self):
        self.remove_temporary_file()

    def create_temporary_file(self, csv_data):
        with tempfile.NamedTemporaryFile(mode='w', delete=False) as temp_csv_file:
            csv_writer = csv.writer(temp_csv_file)
            csv_writer.writerows(csv_data)
            temp_csv_file.close()
            self.temp_csv_file_path = temp_csv_file.name

    def remove_temporary_file(self):
        if hasattr(self, 'temp_csv_file_path') and os.path.exists(self.temp_csv_file_path):
            os.remove(self.temp_csv_file_path)

    def test__existing_file(self):
        csv_data = [
            ['w', 'x', 'y', 'z'],
            ['1142', '1060', '325', '201'],
            ['863', '995', '98', '98'],
            ['1065', '3205', '23', '162'],
            ['554', '120', '0', '54']
        ]
        self.create_temporary_file(csv_data)
        expected_message = "Success: File loaded."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        self.assertEqual(result["message"], expected_message)

    def test__non_existing_file(self):
        non_existing_file_path = 'non_existing_file.csv'
        expected_message = "Error: File not found. Please enter a existing file path."
        result = self.multivar_reg.read_input_data(non_existing_file_path, '3')
        self.assertEqual(result["message"], expected_message)

        non_existing_file_path = '/home/samuil/non_existing_file.csv'
        result = self.multivar_reg.read_input_data(non_existing_file_path, '3')
        self.assertEqual(result["message"], expected_message)

    def test__empty_file(self):
        csv_data = [['w', 'x', 'y', 'z']]
        self.create_temporary_file(csv_data)
        expected_message = "Error: The CSV file is empty."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        self.assertEqual(result["message"], expected_message)

    def test__duplicate_variable_names(self):
        csv_data = [
            ['w', 'w', 'y', 'z'], 
            ['1142', '1060', '325', '201'], 
            ['863', '995', '98', '98']
        ]
        self.create_temporary_file(csv_data)
        expected_message = "Error: The CSV file contains duplicate variable names."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        self.assertEqual(result["message"], expected_message)

    def test__non_alphabet_variable_names(self):
        csv_data = [
            ['w', 'x', '38.23', 'z'], 
            ['1142', '1060', '325', '201'], 
            ['863', '995', '98', '98']
        ]
        self.create_temporary_file(csv_data)
        expected_message = "Error: CSV file column names contain numeric values."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        self.assertEqual(result["message"], expected_message)

    def test__rows_with_missing_data(self):
        csv_data = [
            ['w', 'x', 'y', 'z'], 
            ['1142', '1060', '201'], 
            ['863', '98', '98'], 
            ['863', '995', '98', '98']
        ]
        self.create_temporary_file(csv_data)
        expected_message = "Error: Rows [1, 2] contains missing data."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        self.assertEqual(result["message"], expected_message)

    def test__rows_with_non_numeric_data(self):
        csv_data = [
            ['w', 'x', 'y', 'z'], 
            ['1142', 'repent', '1060', '201'], 
            ['863', '98', '1243', '98'], 
            ['863', '995', 'beleive in the gospel', '98']
        ]
        self.create_temporary_file(csv_data)
        expected_message = "Error: Rows [1, 3] contains non-numeric data."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        self.assertEqual(result["message"], expected_message)

    def test__more_independent_vars_than_input_columns(self):
        csv_data = [
            ['w', 't', 'y', 'z'], 
            ['1142', '1060', '325', '201'], 
            ['863', '995', '98', '98']
        ]
        self.create_temporary_file(csv_data)
        expected_message = "Error: The CSV contains less columns than the input count of independent varaibles."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '4')
        self.assertEqual(result["message"], expected_message)

    def test__negative_count_of_independent_vars(self):
        csv_data = [
            ['w', 't', 'y', 'z'], 
            ['1142', '1060', '325', '201'], 
            ['863', '995', '98', '98']
        ]
        self.create_temporary_file(csv_data)
        expected_message = "Error: Count of independent variables must be greater than zero."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '-10')
        self.assertEqual(result["message"], expected_message)

    def test__valid_count_of_independent_vars(self):
        csv_data = [
            ['w', 't', 'y', 'z'], 
            ['1142', '1060', '325', '201'], 
            ['863', '995', '98', '98']
        ]
        self.create_temporary_file(csv_data)
        expected_message = "Success: File loaded."
        result = self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        self.assertEqual(result["message"], expected_message)

    def test__calculate_first_row(self):
        csv_data = [
            ['w', 't', 'y', 'z'], 
            ['1142', '1060', '325', '201'], 
            ['863', '995', '98', '98']
        ]
        self.create_temporary_file(csv_data)
        expected_result = [2, 2005, 2055, 423, 299]
        self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        result = self.multivar_reg._calculate_first_row(self.multivar_reg.dataset)
        self.assertEqual(result, expected_result)

    def test__calculate_remaining_rows(self):
        csv_data = [
            ['w', 't', 'y', 'z'], 
            ['1142', '1060', '325', '201'], 
            ['863', '995', '98', '98'], 
            ['10', '20', '30', '40']
        ]
        self.create_temporary_file(csv_data)
        expected_result = [
            [2015, 2049033, 2069405, 456024, 314516], 
            [2075, 2069405, 2114025, 442610, 311370], 
            [453, 456024, 442610, 116129, 76129]
        ]
        self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        result = self.multivar_reg._calculate_second_to_last_rows(self.multivar_reg.dataset)
        self.assertEqual(result, expected_result)

    def test__get_max_abs_row(self):
        csv_data = [
            ['w', 't', 'y', 'z'], 
            ['863', '995', '98', '98'], 
            ['10', '20', '30', '40'], 
            ['1142', '1060', '325', '201']
        ]
        self.create_temporary_file(csv_data)
        self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        rows_count = len(self.multivar_reg.dataset)
        pivot_row = 0
        expected_result = 2  # 1142 has the largest abs value in the first column
        result = self.multivar_reg._get_max_abs_row(rows_count, self.multivar_reg.dataset, pivot_row)
        self.assertEqual(result, expected_result)

    def test__swap_max_abs_row_and_pivot_row(self):
        csv_data = [
            ['w', 't', 'y', 'z'], 
            ['863', '995', '98', '98'], 
            ['10', '20', '30', '40'], 
            ['1142', '1060', '325', '201']
        ]
        self.create_temporary_file(csv_data)
        self.multivar_reg.read_input_data(self.temp_csv_file_path, '3')
        rows_count = len(self.multivar_reg.dataset)
        pivot_row = 0
        max_abs_value_row_index = self.multivar_reg._get_max_abs_row(rows_count, self.multivar_reg.dataset, pivot_row)
        expected_gauss_matrix = [
            ['w', 't', 'y', 'z'], 
            [1142, 1060, 325, 201], 
            [10, 20, 30, 40], 
            [863, 995, 98, 98]
        ]
        expected_gauss_matrix_df = pd.DataFrame(expected_gauss_matrix[1:], columns=expected_gauss_matrix[0])
        result_gauss_matrix = self.multivar_reg._swap_max_abs_row_and_pivot_row(self.multivar_reg.dataset, max_abs_value_row_index, pivot_row)
        self.assertTrue(result_gauss_matrix.equals(expected_gauss_matrix_df))

    def test__is_numeric_column(self):
        self.assertTrue(self.multivar_reg._is_numeric_column('A1'))
        self.assertFalse(self.multivar_reg._is_numeric_column('ABC'))

    def test__contains_non_numeric_data(self):
        self.assertTrue(self.multivar_reg._contains_non_numeric_data(pd.Series(['a', 'b', 'c'])))
        self.assertFalse(self.multivar_reg._contains_non_numeric_data(pd.Series([1, 2, 3])))

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_beta_terms(self, mock_stdout):
        self.multivar_reg.result = [1.23456789, 2.3456789, 3.456789, 4.56789]
        self.multivar_reg.print_beta_terms(4)
        expected_output = """########################
### BETA TERMS
########################
b0: 1.2345
b1: 2.3456
b2: 3.4567
b3: 4.5678
"""
        self.assertEqual(mock_stdout.getvalue(), expected_output)