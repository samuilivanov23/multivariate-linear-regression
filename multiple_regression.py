#!/usr/bin/python3
import os
import pandas as pd

DIGITS_AFTER_DECIMAL_POINT = 5

class MultivariateRegression:
    def __init__(self):
        self.dataset = None
        self.gauss_matrix_df = None
        self.result = None
    
    def _calculate_first_row(self, dataset):
        rows = self.dataset.shape[0]
        first_row = [rows]

        for col_name in self.dataset.columns:
            first_row.append(self.dataset[col_name].sum())
        
        return first_row
    
    def _calculate_second_to_last_rows(self, dataset):
        cols = self.dataset.shape[1]
        second_to_last_rows = []

        for i in range(cols - 1):
            current_row = [self.dataset.iloc[:, i].sum()]
            for j in range(cols):
                current_row.append((self.dataset.iloc[:, i] * self.dataset.iloc[:, j]).sum())
            second_to_last_rows.append(current_row)
        
        return second_to_last_rows
    
    def _calculate_starting_gaussian_matrix(self):
        first_row = self._calculate_first_row(self.dataset)
        matrix = self._calculate_second_to_last_rows(self.dataset)
        matrix.insert(0, first_row)
        self.gauss_matrix_df = pd.DataFrame(matrix, dtype=float)

    def _get_max_abs_row(self, rows_count, gauss_matrix_df, pivot_row):
        max_abs_value = 0
        max_abs_value_row_index = pivot_row
        for k in range(pivot_row, rows_count):
            abs_value = abs(gauss_matrix_df.iloc[k, pivot_row])
            if abs_value > max_abs_value:
                max_abs_value = abs_value
                max_abs_value_row_index = k

        return max_abs_value_row_index
    
    def _swap_max_abs_row_and_pivot_row(self, gauss_matrix_df, max_abs_value_row_index, pivot_row):
        gauss_matrix_df.iloc[[pivot_row, max_abs_value_row_index]] = gauss_matrix_df.iloc[[max_abs_value_row_index, pivot_row]]
        
        return gauss_matrix_df
    
    def _eliminate_coefficients_below_pivot_row(self, rows_count, gauss_matrix_df, pivot_row):
        for j in range(pivot_row + 1, rows_count):
            ratio = gauss_matrix_df.iloc[j, pivot_row] / gauss_matrix_df.iloc[pivot_row, pivot_row]
            gauss_matrix_df.iloc[j] -= ratio * gauss_matrix_df.iloc[pivot_row]
        
        return gauss_matrix_df
    
    def _back_substitution(self, rows_count, gauss_matrix_df):
        result = [0] * rows_count
        for i in range(rows_count - 1, -1, -1):
            sum_value = 0
            for j in range(i + 1, rows_count):
                sum_value += gauss_matrix_df.iloc[i, j] * result[j]
            result[i] = (gauss_matrix_df.iloc[i, rows_count] - sum_value) / gauss_matrix_df.iloc[i, i]
        
        return result

    def _perform_gauss_elimination(self):
        rows_count = len(self.gauss_matrix_df)
        for pivot_row in range(rows_count):
            max_abs_value_row_index = self._get_max_abs_row(rows_count, self.gauss_matrix_df, pivot_row)
            self.gauss_matrix_df = self._swap_max_abs_row_and_pivot_row(self.gauss_matrix_df, max_abs_value_row_index, pivot_row)
            self.gauss_matrix_df = self._eliminate_coefficients_below_pivot_row(rows_count, self.gauss_matrix_df, pivot_row)
        
        self.result = self._back_substitution(rows_count, self.gauss_matrix_df)
    
    def calculate_beta_terms(self):
        self._calculate_starting_gaussian_matrix()
        self._perform_gauss_elimination()
    
    @staticmethod
    def convert_to_output_format(number, digits_after_decimal_point):
        number_str = str(number)
        return number_str[:number_str.index('.') + 1 + digits_after_decimal_point]
    
    def print_beta_terms(self, digits_after_decimal_point):
        print("########################")
        print("### BETA TERMS")
        print("########################")
        b0, b1, b2, b3 = self.result
        print("b0: " + self.convert_to_output_format(b0, digits_after_decimal_point))
        print("b1: " + self.convert_to_output_format(b1, digits_after_decimal_point))
        print("b2: " + self.convert_to_output_format(b2, digits_after_decimal_point))
        print("b3: " + self.convert_to_output_format(b3, digits_after_decimal_point))

    def read_input_data(self, file_path, independent_vars_count):
        try:
            # TC_002 Check if file exists on filesystem
            if not os.path.exists(file_path):
                raise FileNotFoundError
            
            self.dataset = pd.read_csv(file_path)

            # TC_009 Check if <count of columns - 1> is less than count of <independent variables>
            # count of columns - 1: because there should be one extra variable that will be predicted
            if (len(self.dataset.columns)-1) < int(independent_vars_count):
                raise ValueError("Error: The CSV contains less columns than the input count of independent varaibles.")
            
            # TC_010 Check if <count of columns> is less than zero
            if int(independent_vars_count) < 0:
                raise ValueError("Error: Count of independent variables must be greater than zero.")

            # TC_003 Check if file is empty
            if self.dataset.empty:
                raise ValueError("Error: The CSV file is empty.")

            # TC_006 Check if file contains duplicate variable names
            # Extract base column names (without suffixes)
            base_column_names = [col.split('.')[0] for col in self.dataset.columns]
            if len(set(base_column_names)) != len(base_column_names):
                raise ValueError("Error: The CSV file contains duplicate variable names.")

            # TC_004 and TC_005 Check if the column names contain only alphabet characters
            if any(self._is_numeric_column(name) for name in self.dataset.columns):
                raise ValueError("Error: CSV file column names contain numeric values.")
            
            # TC_007 Check if there is missing data in some row:col position
            # TC_008 Check if there is row with non-numeric values
            rows_with_missing_data = []
            rows_with_non_numeric_data = []
            for index, row in self.dataset.iterrows():
                if row.isnull().any():
                    rows_with_missing_data.append(index+1)
                elif self._contains_non_numeric_data(row):
                    rows_with_non_numeric_data.append(index+1)

            if rows_with_missing_data:
                raise ValueError(f"Error: Rows {rows_with_missing_data} contains missing data.")
            if rows_with_non_numeric_data:
                raise ValueError(f"Error: Rows {rows_with_non_numeric_data} contains non-numeric data.")

            return {'success_status': True, 'message': "Success: File loaded."}

        except FileNotFoundError:
            self.dataset = None
            return {"success_status": False, "message": "Error: File not found. Please enter a existing file path."}
        except ValueError as value_error_message:
            self.dataset = None
            return {"success_status": False, "message": str(value_error_message)}
    
    def _is_numeric_column(self, column_name):
        return any(char.isdigit() for char in column_name)

    def _contains_non_numeric_data(self, row):
        try:
            pd.to_numeric(row)
            return False
        except ValueError:
            return True

# Usage example
if __name__ == "__main__":
    multivariate_regression = MultivariateRegression()

    while True:
        print("#######################")
        independent_vars_count = input("Enter count of independent variables: ")
        file_path = input("Enter the path to the input CSV file: ")
        read_data = multivariate_regression.read_input_data(file_path, independent_vars_count)
        if read_data["success_status"]:
            break
        else:
            print(read_data["message"])

    multivariate_regression.calculate_beta_terms()
    multivariate_regression.print_beta_terms(DIGITS_AFTER_DECIMAL_POINT)